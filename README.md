# Central de suporte ao Help Desk
Registre aqui os agendamentos, vouchers, feedbacks e outros elementos de nossa plataforma que precisem de alguma atenção da equipe de TI. 

**Ex:** Excluir avaliação que possui o id `5adf446de3ee3f0fd4150339`

&nbsp;
## Como Reportar
Você pode adicionar uma issue de 2 formas:

    1 - Enviando um email para: incoming+SinguDevCrew/help-desk@incoming.gitlab.com 
    2 - Diretamente no painel de issues https://gitlab.com/SinguDevCrew/help-desk/issues